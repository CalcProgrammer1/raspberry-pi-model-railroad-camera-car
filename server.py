from flask import Flask, render_template
import os

app = Flask(__name__)

@app.route('/')

def index():
  return render_template('index.html')

@app.route('/servo-80/')
def servo_80():
  print ('Servo 80%')
  os.system ('sh set_servo.sh 80')
  return render_template('index.html')

@app.route('/servo-70/')
def servo_70():
  print ('Servo 70%')
  os.system ('sh set_servo.sh 70')
  return render_template('index.html')

@app.route('/servo-60/')
def servo_60():
  print ('Servo 60%')
  os.system ('sh set_servo.sh 60')
  return render_template('index.html')

@app.route('/servo-50/')
def servo_50():
  print ('Servo 50%')
  os.system ('sh set_servo.sh 50')
  return render_template('index.html')

@app.route('/servo-40/')
def servo_40():
  print ('Servo 40%')
  os.system ('sh set_servo.sh 40')
  return render_template('index.html')

@app.route('/servo-30/')
def servo_30():
  print ('Servo 30%')
  os.system ('sh set_servo.sh 30')
  return render_template('index.html')

@app.route('/servo-20/')
def servo_20():
  print ('Servo 20%')
  os.system ('sh set_servo.sh 20')
  return render_template('index.html')

if __name__ == '__main__':
  os.system ('sh init_servo.sh')
  os.system ('sh set_servo.sh 50')
  app.run(host="0.0.0.0", port=8000, debug=False)
