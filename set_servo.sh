#! /bin/bash

# Argument is new servo position
SERVO_POS=`cat servo_pos.txt`
NEW_SERVO_POS=$1

while [ "$NEW_SERVO_POS" -gt "$SERVO_POS" ]
do
	SERVO_POS=$((SERVO_POS + 1))
	echo "0=$SERVO_POS%" > /dev/servoblaster
	sleep 0.1
done

while [ "$NEW_SERVO_POS" -lt "$SERVO_POS" ]
do
	SERVO_POS=$((SERVO_POS - 1))
	echo "0=$SERVO_POS%" > /dev/servoblaster
	sleep 0.1
done

echo $SERVO_POS > servo_pos.txt
